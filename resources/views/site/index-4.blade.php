<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from tlsavings.xyz/Savings/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Jan 2018 00:44:26 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>Insurance</title>
	<base  />
			<meta name="viewport" content="width=992" />
		<meta name="description" content="" />
	<meta name="keywords" content="" />
	<!-- Facebook Open Graph -->
	<meta name="og:title" content="Insurance" />
	<meta name="og:description" content="" />
	<meta name="og:image" content="" />
	<meta name="og:type" content="article" />
	<meta name="og:url" content="savings')}}"/>
	<!-- Facebook Open Graph end -->
		
	<link href="public/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<script src="public/css/jquery-1.11.3.min.js" type="text/javascript"></script>
	<script src="public/css/bootstrap.min.js" type="text/javascript"></script>
	<script src="public/css/mainde0d.js?v=20171208161001" type="text/javascript"></script>

	<link href="public/css/font-awesome.min3e6e.css?v=4.7.0" rel="stylesheet" type="text/css" />
	<link href="public/css/site2e13.css?v=20180103162959" rel="stylesheet" type="text/css" />
	<link href="public/css/common0969.css?ts=1515221880" rel="stylesheet" type="text/css" />
	<link href="public/css/50969.css?ts=1515221880" rel="stylesheet" type="text/css" />
	
	<script type="text/javascript">var currLang = '';</script>	
	<!--[if lt IE 9]>
	<script src="public/css/html5shiv.min.js"></script>
	<![endif]-->
</head>


<body><div class="root"><div class="vbox wb_container" id="wb_header">
	
<div class="wb_cont_inner"><div id="wb_element_instance58" class="wb_element wb-menu"><ul class="hmenu"><li><a href="{{url('index-2')}}"target="_self" title="Home">Home</a></li><li><a href="{{url('index-3')}}"target="_self" title="About us">About us</a></li><li class="active"><a href="{{url('index-4')}}"target="_self" title="Savings">Savings</a></li><li><a href="{{url('index-5')}}"target="_self" title="Contacts">Contacts</a></li><li><a href="http://beta.proofdashboard.com/" target="_blank" title="Wallet">Wallet</a></li><li><a href="{{url('index-6')}}"target="_blank" title="NUMA">NUMA</a></li>@guest <li><a href="{{url('/login')}}" target="_blank" title="Login">LOGIN</a></li><li><a href="{{url('/join')}}" target="_blank" title="Join">JOIN</a></li> @endguest<li><a href="http://touchinglivesskills.xyz/" target="_blank" title="Empowerment">Empowerment</a></li></ul><div class="clearfix"></div></div><div id="wb_element_instance59" class="wb_element wb_element_shape"><div class="wb_shp"></div></div><div id="wb_element_instance60" class="wb_element" style=" line-height: normal;"><h1 class="wb-stl-heading1"><span style="background-color:#ffffff;">TLSavings</span></h1>
</div><div id="wb_element_instance61" class="wb_element wb_element_picture"><img alt="gallery/tlsavings jpg" src="public/jpg/f7923973432d866a679e08b1e952e7ec_80x80.jpg"></div></div><div class="wb_cont_outer"></div><div class="wb_cont_bg"></div></div>
<div class="vbox wb_container" id="wb_main">
	
<div class="wb_cont_inner"><div id="wb_element_instance63" class="wb_element" style=" line-height: normal;"><p class="wb-stl-normal">TLSavings offers a wide range of investment opportunities through the savings platform. They have been specially arranged to suit your savings need and experience. Our various savings platform is packaged for both Individuals and Corporate bodies. Our products are divided into Individual Savings Account and Corporate Savings Account and they are:</p>

<p class="wb-stl-normal"><u>Individual Savings Account</u></p>

<p class="wb-stl-normal">A) Special Savings Account</p>

<p class="wb-stl-normal">B) Investment Savings Account</p>

<p class="wb-stl-normal">C) Thrift Savings Account</p>

<p class="wb-stl-normal">D) Empowerment Savings (EP Account)</p>

<p class="wb-stl-normal">E) Credit Savings Account</p>

<p class="wb-stl-normal"><u>Corporate Accounts</u></p>

<p class="wb-stl-normal">F) Corporate Savings Account</p>

<p class="wb-stl-normal">G) Corporate Investment Savings Account</p>

<p class="wb-stl-normal">H) Corporate Investment Savings Account</p>

<p class="wb-stl-normal">I) Corporate thrift Savings Account</p>

<p class="wb-stl-normal">J) Corporate Credit Account</p>

<p class="wb-stl-normal"> </p>

<p class="wb-stl-normal">To own any of our Savings Account Visit any of our branch or agent branch or call +234 706 533 7887 or click the botton below to apply</p>
</div><div id="wb_element_instance64" class="wb_element" style=" line-height: normal;"><h1 class="wb-stl-heading1">Savings</h1>
</div><div id="wb_element_instance65" class="wb_element"><a class="wb_button" href="{{url('index-7')}}"title="Account Form" target="_blank"><span>APPLY</span></a></div><div id="wb_element_instance66" class="wb_element" style="width: 100%;">
						<script type="text/javascript">
				$(function() {
					$("#wb_element_instance66").hide();
				});
			</script>
						</div></div><div class="wb_cont_outer"></div><div class="wb_cont_bg"></div></div>
<div class="vbox wb_container" id="wb_footer">
	
<div class="wb_cont_inner" style="height: 104px;"><div id="wb_element_instance62" class="wb_element" style=" line-height: normal;"><p class="wb-stl-footer">© 2018 <a href="{{url('index')}}>tlsavings.xyz</a></p></div><div id="wb_element_instance67" class="wb_element" style="text-align: center; width: 100%;"><div class="wb_footer"></div><script type="text/javascript">
			$(function() {
				var footer = $(".wb_footer");
				var html = (footer.html() + "").replace(/^\s+|\s+$/g, "");
				if (!html) {
					footer.parent().remove();
					footer = $("#wb_footer, #wb_footer .wb_cont_inner");
					footer.css({height: ""});
				}
			});
			</script></div></div><div class="wb_cont_outer"></div><div class="wb_cont_bg"></div></div><div class="wb_sbg"></div></div></body>

<!-- Mirrored from tlsavings.xyz/Savings/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Jan 2018 00:44:26 GMT -->
</html>
