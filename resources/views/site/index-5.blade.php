<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from tlsavings.xyz/Contacts/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Jan 2018 00:44:27 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=utf-8"/><!-- /Added by HTTrack -->
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>Contacts</title>
    <base/>
    <meta name="viewport" content="width=992"/>
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <!-- Facebook Open Graph -->
    <meta name="og:title" content="Contacts"/>
    <meta name="og:description" content=""/>
    <meta name="og:image" content=""/>
    <meta name="og:type" content="article"/>
    <meta name="og:url" content="contacts')}}"/>
    <!-- Facebook Open Graph end -->

    <link href="public/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <script src="public/css/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="public/css/bootstrap.min.js" type="text/javascript"></script>
    <script src="public/css/mainde0d.js?v=20171208161001" type="text/javascript"></script>

    <link href="public/css/font-awesome.min3e6e.css?v=4.7.0" rel="stylesheet" type="text/css"/>
    <link href="public/css/site2e13.css?v=20180103162959" rel="stylesheet" type="text/css"/>
    <link href="public/css/common0969.css?ts=1515221880" rel="stylesheet" type="text/css"/>
    <link href="public/css/30969.css?ts=1515221880" rel="stylesheet" type="text/css"/>
    <style type="text/css">.wb_form_site th, .wb_form_site td {
            display: table-cell !important;
        }

        .wb_form_site tr {
            display: table-row !important;
        }</style>

    <script type="text/javascript">var currLang = '';</script>
    <!--[if lt IE 9]>
    <script src="public/css/html5shiv.min.js"></script>
    <![endif]-->
</head>


<body>
<div class="root">
    <div class="vbox wb_container" id="wb_header">

        <div class="wb_cont_inner">
            <div id="wb_element_instance46" class="wb_element wb-menu">
                <ul class="hmenu">
                    <li class="active"><a href="{{url('index-2')}}" target="_self" title="Home">Home</a></li>
                    <li><a href="{{url('index-5')}}" target="_self" title="Contacts">Contacts</a></li>
                    <li><a href="{{url('index-6')}}" target="_blank" title="NUMA">NUMA</a></li>

                    @guest
                        <li><a href="{{url('/login')}}" target="_blank" title="Login">LOGIN</a></li>
                        <li><a href="{{url('/join')}}" target="_blank" title="Join">JOIN</a></li> @endguest
                    @auth
                        <li><a href="{{url('/dashboard')}}" target="_blank" title="Dashboard">Dashboard</a></li>@endauth
                    <li><a href="http://touchinglivesskills.xyz/" target="_blank" title="Empowerment">Empowerment</a>
                    </li>

                </ul>
                <div class="clearfix"></div>
            </div>
            <div id="wb_element_instance47" class="wb_element wb_element_shape">
                <div class="wb_shp"></div>
            </div>
            <div id="wb_element_instance48" class="wb_element" style=" line-height: normal;"><h1
                        class="wb-stl-heading1"><span style="background-color:#ffffff;">TLSavings</span></h1>
            </div>
            <div id="wb_element_instance49" class="wb_element wb_element_picture"><img alt="gallery/tlsavings jpg"
                                                                                       src="public/jpg/f7923973432d866a679e08b1e952e7ec_80x80.jpg">
            </div>
        </div>
        <div class="wb_cont_outer"></div>
        <div class="wb_cont_bg"></div>
    </div>
    <div class="vbox wb_container" id="wb_main">

        <div class="wb_cont_inner">
            <div id="wb_element_instance51" class="wb_element" style=" line-height: normal;"><h1
                        class="wb-stl-heading1">Contacts</h1></div>
            <div id="wb_element_instance52" class="wb_element" style=" line-height: normal;"><p class="wb-stl-normal">
                    Please fill the form below:</p>
            </div>
            <div id="wb_element_instance53" class="wb_element wb-map">
                <div style="background: rgba(0,0,0,0.38); position: absolute; top: 0; left: 0; width: 100%; height: 100%;">
                    <div style="font-size: 24px; width: 100%; color: #c00; padding: 0 20%; text-align: center; display: inline-block; vertical-align: middle;">
                        Get API Key from <a
                                style="display: inline-block; max-width: 100%; word-break: break-all; color: #fff;"
                                target="_blank"
                                href="https://developers.google.com/maps/documentation/javascript/get-api-key">https://developers.google.com/maps/documentation/javascript/get-api-key</a>
                    </div>
                    <div style="vertical-align: middle; height: 100%; display: inline-block;"></div>
                </div>
                <script type="text/javascript">
                    (function () {
                        var resizeFunc = function () {
                            var elem = $("#wb_element_instance53");
                            var w = elem.width(), h = elem.height();
                            elem.find("div > div:first").css("zoom", Math.max(0.5, Math.min(1, ((w * h) / 120000))));
                        };
                        $(window).on("resize", resizeFunc);
                        resizeFunc();
                    })();
                </script>
            </div>
            <div id="wb_element_instance54" class="wb_element">
                <form class="wb_form wb_form_site" method="post" enctype="multipart/form-data"><input type="hidden"
                                                                                                      name="wb_form_id"
                                                                                                      value="58e37a0e"><textarea
                            name="message" rows="3" cols="20" class="hpc"></textarea>
                    <table>
                        <tr>
                            <th class="wb-stl-normal">Name&nbsp;&nbsp;</th>
                            <td><input type="hidden" name="wb_input_0" value="Name"><input
                                        class="form-control form-field" type="text" value="" name="wb_input_0"
                                        required="required"></td>
                        </tr>
                        <tr>
                            <th class="wb-stl-normal">E-mail&nbsp;&nbsp;</th>
                            <td><input type="hidden" name="wb_input_1" value="E-mail"><input
                                        class="form-control form-field" type="text" value="" name="wb_input_1"
                                        required="required"></td>
                        </tr>
                        <tr class="area-row">
                            <th class="wb-stl-normal">Message&nbsp;&nbsp;</th>
                            <td><input type="hidden" name="wb_input_2" value="Message"><textarea
                                        class="form-control form-field form-area-field" rows="3" cols="20"
                                        name="wb_input_2" required="required"></textarea></td>
                        </tr>
                        <tr class="form-footer">
                            <td colspan="2">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </td>
                        </tr>
                    </table>
                </form>
                <script type="text/javascript">
                </script>
            </div>
            <div id="wb_element_instance55" class="wb_element" style=" line-height: normal;"><p class="wb-stl-normal">
                    <strong>Address:</strong></p>

                <p class="wb-stl-normal">2 Atakpo Road, Uyo</p>

                <p class="wb-stl-normal"><strong>Phone: </strong></p>

                <p class="wb-stl-normal"><span dir="ltr" style="direction: ltr;">+2347065337887</span></p>

                <p class="wb-stl-normal"><strong>E-mail:</strong></p>

                <p class="wb-stl-normal"><a data-type="email" data-url="info@site.info" href="mailto:info@site.info">info@</a>tlsavings.xyz
                </p>
            </div>
            <div id="wb_element_instance56" class="wb_element" style="width: 100%;">
                <script type="text/javascript">
                    $(function () {
                        $("#wb_element_instance56").hide();
                    });
                </script>
            </div>
        </div>
        <div class="wb_cont_outer"></div>
        <div class="wb_cont_bg"></div>
    </div>
    <div class="vbox wb_container" id="wb_footer">

        <div class="wb_cont_inner" style="height: 104px;">
            <div id="wb_element_instance50" class="wb_element" style=" line-height: normal;"><p class="wb-stl-footer">©
                    2018 <a href="{{url('index')}}>tlsavings.xyz</a></p></div><div id="
                            wb_element_instance57" class="wb_element" style="text-align: center; width:
                    100%;">
                <div class="wb_footer"></div>
                <script type="text/javascript">
                    $(function () {
                        var footer = $(".wb_footer");
                        var html = (footer.html() + "").replace(/^\s+|\s+$/g, "");
                        if (!html) {
                            footer.parent().remove();
                            footer = $("#wb_footer, #wb_footer .wb_cont_inner");
                            footer.css({height: ""});
                        }
                    });
                </script>
            </div>
        </div>
        <div class="wb_cont_outer"></div>
        <div class="wb_cont_bg"></div>
    </div>
    <div class="wb_sbg"></div>
</div>
</body>

<!-- Mirrored from tlsavings.xyz/Contacts/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Jan 2018 00:44:27 GMT -->
</html>
