<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from tlsavings.xyz/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Jan 2018 00:44:19 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=utf-8"/><!-- /Added by HTTrack -->
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>HOME</title>
    <base/>
    <meta name="viewport" content="width=992"/>
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <!-- Facebook Open Graph -->
    <meta name="og:title" content="HOME"/>
    <meta name="og:description" content=""/>
    <meta name="og:image" content=""/>
    <meta name="og:type" content="article"/>
    <meta name="og:url" content="index')}}"/>
    <!-- Facebook Open Graph end -->

    <link href="public/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <script src="public/css/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="public/css/bootstrap.min.js" type="text/javascript"></script>
    <script src="public/css/mainde0d.js?v=20171208161001" type="text/javascript"></script>

    <link href="public/css/font-awesome.min3e6e.css?v=4.7.0" rel="stylesheet" type="text/css"/>
    <link href="public/css/site2e13.css?v=20180103162959" rel="stylesheet" type="text/css"/>
    <link href="public/css/common0969.css?ts=1515221880" rel="stylesheet" type="text/css"/>
    <link href="public/css/10969.css?ts=1515221880" rel="stylesheet" type="text/css"/>

    <script type="text/javascript">var currLang = '';</script>
    <!--[if lt IE 9]>
    <script src="public/css/html5shiv.min.js"></script>
    <![endif]-->
</head>


<body>
<div class="root">
    <div class="vbox wb_container" id="wb_header">

        <div class="wb_cont_inner">
            <div id="wb_element_instance0" class="wb_element wb-menu">
                <ul class="hmenu">
                    <li class="active"><a href="{{url('index-2')}}" target="_self" title="Home">Home</a></li>
                    <li><a href="{{url('index-5')}}" target="_self" title="Contacts">Contacts</a></li>
                    <li><a href="{{url('index-6')}}" target="_blank" title="NUMA">NUMA</a></li>

                    @guest
                        <li><a href="{{url('/login')}}" target="_blank" title="Login">LOGIN</a></li>
                        <li><a href="{{url('/join')}}" target="_blank" title="Join">JOIN</a></li> @endguest
                    @auth
                        <li><a href="{{url('/dashboard')}}" target="_blank" title="Dashboard">Dashboard</a></li>@endauth
                    <li><a href="http://touchinglivesskills.xyz/" target="_blank" title="Empowerment">Empowerment</a>
                    </li>

                </ul>
                <div class="clearfix"></div>
            </div>
            <div id="wb_element_instance1" class="wb_element wb_element_shape">
                <div class="wb_shp"></div>
            </div>
            <div id="wb_element_instance2" class="wb_element" style=" line-height: normal;"><h1 class="wb-stl-heading1">
                    <span style="background-color:#ffffff;">TLSavings</span></h1>
            </div>
            <div id="wb_element_instance3" class="wb_element wb_element_picture"><img alt="gallery/tlsavings jpg"
                                                                                      src="public/jpg/f7923973432d866a679e08b1e952e7ec_80x80.jpg">
            </div>
        </div>
        <div class="wb_cont_outer"></div>
        <div class="wb_cont_bg"></div>
    </div>
    <div class="vbox wb_container" id="wb_main">

        <div class="wb_cont_inner">
            <div id="wb_element_instance5" class="wb_element wb_element_shape">
                <div class="wb_shp"></div>
            </div>
            <div id="wb_element_instance6" class="wb_element wb_element_shape">
                <div class="wb_shp"></div>
            </div>
            <div id="wb_element_instance7" class="wb_element wb_element_shape">
                <div class="wb_shp"></div>
            </div>
            <div id="wb_element_instance8" class="wb_element wb_element_shape">
                <div class="wb_shp"></div>
            </div>
            <div id="wb_element_instance9" class="wb_element wb_element_picture"><img alt="gallery/life"
                                                                                      src="public/png/bcb158c0a5fa6640a4ae37a538770a93.png">
            </div>
            <div id="wb_element_instance10" class="wb_element wb_element_picture"><img alt="gallery/car-"
                                                                                       src="public/png/82ce2872754da9eb53cf675990d15e63.png">
            </div>
            <div id="wb_element_instance11" class="wb_element wb_element_picture"><img alt="gallery/home"
                                                                                       src="public/png/f8725e5056724a5c51e6dd12e3910aee.png">
            </div>
            <div id="wb_element_instance12" class="wb_element wb_element_picture"><img alt="gallery/travel"
                                                                                       src="public/png/4072ddcc321b8e59cf0cb06edd94b2c7.png">
            </div>
            <div id="wb_element_instance13" class="wb_element" style=" line-height: normal;"><h1 class="wb-stl-heading1"
                                                                                                 style="text-align: center;">
                    About TLSavings</h1>
                <p class="wb-stl-normal" style="text-align: justify;">TLSavings is a subsidiary organization of Touching
                    Lives Skills International with branches spread
                    accross the globe in every nation where Touching Lives Skills Project Exist. 

                </p>
                <p class="wb-stl-normal" style="text-align: justify;">TLSavings official currency is the Pneuma Coin
                    (NUMA COIN). We offer exchange from Numa to other
                    currencies. To learn more about Pneuma visit the NUMA page above.

                </p>
                <p class="wb-stl-normal" style="text-align: justify;">Our vision is to encourage savings and forex. We
                    have several savings and investment opportunities
                    that can to encourage individuals get funds for projects and get good rates on their savings.

                </p>
                <p class="wb-stl-normal" style="text-align: justify;">Our savings platform also gives our clients the
                    opportunity to participate in our Empowerment program
                    and Credit Program. Interested individuals can visit the Empowerment page to get more information.

                </p>
                <p class="wb-stl-normal" style="text-align: justify;">We also have a 24hours support service. Our
                    Dynamic team are kin to ensure that you have an excellent
                    experience while saving with us.</p>
            </div>
        </div>
    </div>
    <div class="vbox wb_container" id="wb_footer">

        <div class="wb_cont_inner" style="height: 104px;">
            <div id="wb_element_instance4" class="wb_element" style=" line-height: normal;"><p class="wb-stl-footer">©
                    2018 <a href="{{url('index')}}>tlsavings.xyz</a></p></div><div id=" wb_element_instance31"
                    class="wb_element" style="text-align: center; width: 100%;">
                <div class="wb_footer"></div>
                <script type="text/javascript">
                    $(function () {
                        var footer = $(".wb_footer");
                        var html = (footer.html() + "").replace(/^\s+|\s+$/g, "");
                        if (!html) {
                            footer.parent().remove();
                            footer = $("#wb_footer, #wb_footer .wb_cont_inner");
                            footer.css({height: ""});
                        }
                    });
                </script>
            </div>
        </div>
        <div class="wb_cont_outer"></div>
        <div class="wb_cont_bg"></div>
    </div>
    <div class="wb_sbg"></div>
</div>
</body>

<!-- Mirrored from tlsavings.xyz/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Jan 2018 00:44:25 GMT -->
</html>
