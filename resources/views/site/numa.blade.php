<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from tlsavings.xyz/NUMA by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Jan 2018 00:44:28 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>NUMA</title>
	<base  />
			<meta name="viewport" content="width=992" />
		<meta name="description" content="" />
	<meta name="keywords" content="" />
	<!-- Facebook Open Graph -->
	<meta name="og:title" content="NUMA" />
	<meta name="og:description" content="" />
	<meta name="og:image" content="" />
	<meta name="og:type" content="article" />
	<meta name="og:url" content="numa')}}"/>
	<!-- Facebook Open Graph end -->
		
	<link href="public/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<script src="public/css/jquery-1.11.3.min.js" type="text/javascript"></script>
	<script src="public/css/bootstrap.min.js" type="text/javascript"></script>
	<script src="public/css/mainde0d.js?v=20171208161001" type="text/javascript"></script>

	<link href="public/css/font-awesome.min3e6e.css?v=4.7.0" rel="stylesheet" type="text/css" />
	<link href="public/css/site2e13.css?v=20180103162959" rel="stylesheet" type="text/css" />
	<link href="public/css/common0969.css?ts=1515221880" rel="stylesheet" type="text/css" />
	<link href="public/css/40969.css?ts=1515221880" rel="stylesheet" type="text/css" />
	
	<script type="text/javascript">var currLang = '';</script>	
	<!--[if lt IE 9]>
	<script src="public/css/html5shiv.min.js"></script>
	<![endif]-->
</head>


<body><div class="root"><div class="vbox wb_container" id="wb_header">
	
<div class="wb_cont_inner"><div id="wb_element_instance68" class="wb_element wb-menu"><ul class="hmenu"><li><a href="{{url('index-2')}}"target="_self" title="Home">Home</a></li><li><a href="{{url('index-3')}}"target="_self" title="About us">About us</a></li><li><a href="{{url('index-4')}}"target="_self" title="Savings">Savings</a></li><li><a href="{{url('index-5')}}"target="_self" title="Contacts">Contacts</a></li><li><a href="http://beta.proofdashboard.com/" target="_blank" title="Wallet">Wallet</a></li><li class="active"><a href="{{url('index-6')}}"target="_blank" title="NUMA">NUMA</a></li>@guest <li><a href="{{url('/login')}}" target="_blank" title="Login">LOGIN</a></li><li><a href="{{url('/join')}}" target="_blank" title="Join">JOIN</a></li> @endguest<li><a href="http://touchinglivesskills.xyz/" target="_blank" title="Empowerment">Empowerment</a></li></ul><div class="clearfix"></div></div><div id="wb_element_instance69" class="wb_element wb_element_shape"><div class="wb_shp"></div></div><div id="wb_element_instance70" class="wb_element" style=" line-height: normal;"><h1 class="wb-stl-heading1"><span style="background-color:#ffffff;">TLSavings</span></h1>
</div><div id="wb_element_instance71" class="wb_element wb_element_picture"><img alt="gallery/tlsavings jpg" src="public/jpg/f7923973432d866a679e08b1e952e7ec_80x80.jpg"></div></div><div class="wb_cont_outer"></div><div class="wb_cont_bg"></div></div>
<div class="vbox wb_container" id="wb_main">
	
<div class="wb_cont_inner"><div id="wb_element_instance73" class="wb_element" style=" line-height: normal;"><p class="wb-stl-normal" style="text-align: justify;">NUMA is from the greek word PNEUMA which means SPIRIT. It is a spirit coin that promotes the spirit to earn value. It was adopted by organizations running skills and empowerment projects because of the quality it represents.</p>

<p class="wb-stl-normal" style="text-align: justify;"> </p>

<p class="wb-stl-normal" style="text-align: justify;">NUMA COIN is an official coin used in the Touching Lives Skills International Community for empowerment. NUMA COIN is owned, possessed, circulating and exchanged by over 1,000,000 Touching Lives Community members and there is a greater demand of it by over 10,000 persons on a daily basis.</p>

<p class="wb-stl-normal" style="text-align: justify;"> </p>

<p class="wb-stl-normal" style="text-align: justify;">TLSavings official currency is the Pneuma Coin (NUMA COIN). We accept other currencies but they are exchanged to Pneuma before payment is made to an individuals account. To learn more about Pneuma visit the NUMA page above.</p>

<p class="wb-stl-normal" style="text-align: justify;">Our vision is to encourage savings and interest on savings. We have several savings opportunity to encourage individuals save funds for big projects and get good interest rates on their savings. To learn more visit the savings page.</p>

<p class="wb-stl-normal" style="text-align: justify;"> </p>

<p class="wb-stl-normal" style="text-align: justify;">Our savings platform also gives our clients the opportunity to earn more NUMA through the participation in our Empowerment program and Credit Program and we provide easy exchange platform for the coin and to the local currency in the country of residence. Interested individuals can visit the Empowerment page to get more information.</p>

<p class="wb-stl-normal" style="text-align: justify;"> </p>

<p class="wb-stl-normal" style="text-align: justify;">We also have a 24hours support services. Our Dynamic team are kin to ensure that you have an excellent experience while saving with us.</p>
</div><div id="wb_element_instance74" class="wb_element" style=" line-height: normal;"><h1 class="wb-stl-heading1">NUMA COIN</h1>
</div><div id="wb_element_instance75" class="wb_element" style=" line-height: normal;"><h1 class="wb-stl-heading1">NUMA COIN CRYPTOCURRENCY</h1>
</div><div id="wb_element_instance76" class="wb_element"><a class="wb_button" href="{{url('index-3')}}><span></span></a></div><div id="wb_element_instance77" class="wb_element"><a class="wb_button" href="{{url('https://beta.proofdashboard.com/users/14689/assets/6506#" target="_blank"><span>LEARN MORE</span></a></div><div id="wb_element_instance78" class="wb_element"><a class="wb_button" href="{{url('https://etherscan.io/address/0x96Cc88f54d28C764436c35B88481d4fb9966D14e#code" title="PNEUMA COIN" target="_blank"><span>View on Ether Scan</span></a></div><div id="wb_element_instance79" class="wb_element"><a name="NUMA%20COIN" class="wb_anchor"></a><a class="wb_button" href="http://www.proofsuite.com/i/OWJdVuW3iX" title="ICO" target="_blank"><span>View ICO</span></a></div><div id="wb_element_instance80" class="wb_element" style="width: 100%;">
						<script type="text/javascript">
				$(function() {
					$("#wb_element_instance80").hide();
				});
			</script>
						</div></div><div class="wb_cont_outer"></div><div class="wb_cont_bg"></div></div>
<div class="vbox wb_container" id="wb_footer">
	
<div class="wb_cont_inner" style="height: 104px;"><div id="wb_element_instance72" class="wb_element" style=" line-height: normal;"><p class="wb-stl-footer">© 2018 <a href="{{url('index')}}>tlsavings.xyz</a></p></div><div id="wb_element_instance81" class="wb_element" style="text-align: center; width: 100%;"><div class="wb_footer"></div><script type="text/javascript">
			$(function() {
				var footer = $(".wb_footer");
				var html = (footer.html() + "").replace(/^\s+|\s+$/g, "");
				if (!html) {
					footer.parent().remove();
					footer = $("#wb_footer, #wb_footer .wb_cont_inner");
					footer.css({height: ""});
				}
			});
			</script></div></div><div class="wb_cont_outer"></div><div class="wb_cont_bg"></div></div><div class="wb_sbg"></div></div></body>

<!-- Mirrored from tlsavings.xyz/NUMA by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Jan 2018 00:44:28 GMT -->
</html>
